'use strict';
module.exports = (sequelize, DataTypes) => {
  const Member = sequelize.define('Member', {
    orgName: DataTypes.STRING,
    username: DataTypes.STRING,
    avatarUrl: DataTypes.STRING,
    followersCount: DataTypes.INTEGER,
    followingCount: DataTypes.INTEGER,
    deletedAt: DataTypes.DATE
  }, {
    indexes: [
      {
        unique: false,
        fields: ['orgName']
      }
    ]
  });
  Member.associate = function(models) {
    // associations can be defined here
  };
  return Member;
};
