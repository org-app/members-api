require('dotenv').config();

module.exports = {
  development: {
    database: 'members_db_dev',
    username: 'postgres',
    password: 'abc123',
    host: '127.0.0.1',
    dialect: 'postgres'
  },
  test: {
    database: 'members_db_test',
    username: 'postgres',
    password: 'abc123',
    host: '127.0.0.1',
    dialect: 'postgres'
  },
  production: {
    database: process.env.DB_NAME,
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    host: process.env.DB_HOST,
    dialect: 'postgres'
  }
};
