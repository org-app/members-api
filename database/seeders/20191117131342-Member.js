'use strict';

const uuid = require('uuid');

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.bulkInsert(
      'Members',
      [
        {
          id: uuid.v4(),
          username: 'auser',
          avatarUrl: null,
          orgName: 'xendit',
          followersCount: 10,
          followingCount: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: uuid.v4(),
          username: 'anotheruser',
          avatarUrl: null,
          orgName: 'xendit',
          followersCount: 9,
          followingCount: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: uuid.v4(),
          username: 'user1',
          avatarUrl: null,
          orgName: 'xendit',
          followersCount: 1000,
          followingCount: 0,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
  ),

  down: (queryInterface, Sequelize) => queryInterface.bulkDelete('Members', null, {}),
};
