FROM node:12.13.0

ARG APP_DIR="/usr/src/app"

RUN mkdir -p $APP_DIR
WORKDIR $APP_DIR

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 8080
CMD [ "npm", "run", "start" ]
