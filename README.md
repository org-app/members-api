## Members API

This API allows members pertaining to a particular organization to be fetched.

### Node Version
12.13.0

### Installing Dependencies
```
npm install
```

### Running Unit Tests
```
npm run test
```

### Setting the environment
Save another copy of .env.sample to .env and modify accordingly.

### Setting up the Database
First, ensure that a database of the same name as that specified in .env exists.

To run migrations, execute:
```
npm run migrate-db
```

To populate contents of tables:
```
npm run seed-db
```

To undo last migration:
```
npm run undo-migrate-db
```

### API Documentation
Refer to https://app.apiary.io/membersapi6/editor
