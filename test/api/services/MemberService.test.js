import chai from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';

chai.use(sinonChai);
const expect = chai.expect;

import database from '../../../database/models';
import MemberService from '../../../api/services/MemberService';

describe('MemberService', () => {
  const orgName = 'xendit';

  describe('getAllByOrgName()', () => {
    let findAllStub;
    const expectedMembers = [
      {
        username: 'auser',
        avatarUrl: null,
        followersCount: 1,
        followingCount: 10
      }
    ];

    afterEach(() => {
      findAllStub.restore();
    });

    describe('when sequelize successfully finds data', () => {
      beforeEach(() => {
        findAllStub = sinon
          .stub(database.Member, 'findAll')
          .returns(Promise.resolve(expectedMembers));
      });

      it('should return data', async () => {
        const actualComments = await MemberService.getAllByOrgName(orgName);

        expect(actualComments).to.eql(expectedMembers);
        expect(findAllStub).to.have.been.calledOnce;
        expect(findAllStub).to.have.been.calledWith({
          where: {
            orgName: orgName,
            deletedAt: null
          },
          attributes: ['username', 'avatarUrl', 'followersCount', 'followingCount'],
          order: [
            ['followersCount', 'DESC']
          ]
        });
      });
    });

    describe('when sequelize throws an error', () => {
      beforeEach(() => {
        findAllStub = sinon
          .stub(database.Member, 'findAll')
          .returns(Promise.reject('error'));
      });

      it('should throw an error', async () => {
        let error;

        try {
          await MemberService.getAllByOrgName(orgName);
        } catch (thrownError) {
          error = thrownError;
        }

        expect(error).to.eql('error');
        expect(findAllStub).to.have.been.calledOnce;
        expect(findAllStub).to.have.been.calledWith({
          where: {
            orgName: orgName,
            deletedAt: null
          },
          attributes: ['username', 'avatarUrl', 'followersCount', 'followingCount'],
          order: [
            ['followersCount', 'DESC']
          ]
        });
      });
    });
  });
});
