import chai from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';

chai.use(sinonChai);
const expect = chai.expect;

import ResponseHelper from '../../../api/helpers/ResponseHelper';
import MemberController from '../../../api/controllers/MemberController';
import MemberService from '../../../api/services/MemberService';
import {ERROR_CODES} from '../../../api/constants/ErrorResponses';

describe('MemberController', () => {
  const res = {};
  const orgName = 'xendit';

  describe('getAllByOrgName()', () => {
    const req = {
      params: {
        orgName: orgName
      },
    };
    const members = [
      {
        username: 'auser',
        avatarUrl: null,
        followersCount: 1,
        followingCount: 10
      }
    ];

    describe('when MemberService returns no error', () => {
      let getStub, successStub;

      beforeEach(() => {
        getStub = sinon
          .stub(MemberService, 'getAllByOrgName')
          .returns(Promise.resolve(members));
        successStub = sinon.stub(ResponseHelper, 'jsonSuccess');
      });

      afterEach(() => {
        getStub.restore();
        successStub.restore();
      });

      it('should call ResponseHelper.success with the result', async () => {
        await MemberController.getAllByOrgName(req, res);

        expect(getStub).to.have.been.calledOnce;
        expect(getStub).to.have.been.calledWith(req.params.orgName);
        expect(successStub).to.have.been.calledOnce;
        expect(successStub).to.have.been.calledWith(res, members);
      });
    });

    describe('when MemberService returns an error', () => {
      let getStub, errorStub;

      beforeEach(() => {
        getStub = sinon
          .stub(MemberService, 'getAllByOrgName')
          .returns(Promise.reject('error'));
        errorStub = sinon.stub(ResponseHelper, 'error');
      });

      afterEach(() => {
        getStub.restore();
        errorStub.restore();
      });

      it('should call ResponseHelper.error with INTERNAL_SERVER_ERROR', async () => {
        await MemberController.getAllByOrgName(req, res);

        expect(getStub).to.have.been.calledOnce;
        expect(getStub).to.have.been.calledWith(req.params.orgName);
        expect(errorStub).to.have.been.calledOnce;
        expect(errorStub).to.have.been.calledWith(res, ERROR_CODES.INTERNAL_SERVER_ERROR);
      });
    });
  });
});
