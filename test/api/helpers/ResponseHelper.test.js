import chai from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';

chai.use(sinonChai);
const expect = chai.expect;

import ResponseHelper from '../../../api/helpers/ResponseHelper';
import {ERROR_CODES, ERROR_RESPONSES} from '../../../api/constants/ErrorResponses';

describe('ResponseHelper', () => {
  const res = {
    json: () => {},
    status: function() {
      return this;
    },
    send: () => {}
  };

  describe('success()', () => {
    let jsonSpy;

    beforeEach(() => {
      jsonSpy = sinon.spy(res, 'json');
    });

    afterEach(() => {
      jsonSpy.restore();
    });

    describe('when json is falsy', () => {
      it('should not call res.json', () => {
        ResponseHelper.jsonSuccess(res, null);

        expect(jsonSpy).to.not.have.been.calledOnce;
      });
    });

    describe('when all inputs are as expected', () => {
      it('should call res.json with json response', () => {
        const json = {foo: 'bar'};

        ResponseHelper.jsonSuccess(res, json);

        expect(jsonSpy).to.have.been.calledOnce;
        expect(jsonSpy).to.have.been.calledWith(json);
      });
    });
  });

  describe('error()', () => {
    let statusSpy, jsonSpy;
    const errorResponse = ERROR_RESPONSES[ERROR_CODES.INTERNAL_SERVER_ERROR];
    const expectedStatus = errorResponse.status;
    const expectedJson = errorResponse.json;

    beforeEach(() => {
      statusSpy = sinon.spy(res, 'status');
      jsonSpy = sinon.spy(res, 'json');
    });

    afterEach(() => {
      statusSpy.restore();
      jsonSpy.restore();
    });

    describe('when error code is falsy', () => {
      it('should call res.status().json() with internal server error', () => {
        ResponseHelper.error(res, null);

        expect(statusSpy).to.have.been.calledOnce;
        expect(statusSpy).to.have.been.calledWith(expectedStatus);
        expect(jsonSpy).to.have.been.calledOnce;
        expect(jsonSpy).to.have.been.calledWith(expectedJson);
      });
    });

    describe('when error code is not in list of error codes', () => {
      it('should call res.status().json() with internal server error', () => {
        ResponseHelper.error(res, -1);

        expect(statusSpy).to.have.been.calledOnce;
        expect(statusSpy).to.have.been.calledWith(expectedStatus);
        expect(jsonSpy).to.have.been.calledOnce;
        expect(jsonSpy).to.have.been.calledWith(expectedJson);
      });
    });

    describe('when error code is in list of error codes', () => {
      it('should call res.status().json() with the correct error', () => {
        ResponseHelper.error(res, ERROR_CODES.INTERNAL_SERVER_ERROR);

        expect(statusSpy).to.have.been.calledOnce;
        expect(statusSpy).to.have.been.calledWith(expectedStatus);
        expect(jsonSpy).to.have.been.calledOnce;
        expect(jsonSpy).to.have.been.calledWith(expectedJson);
      });
    });
  });

  describe('noContent()', () => {
    let statusSpy, sendSpy;

    beforeEach(() => {
      statusSpy = sinon.spy(res, 'status');
      sendSpy = sinon.spy(res, 'send');
    });

    afterEach(() => {
      statusSpy.restore();
      sendSpy.restore();
    });

    it('should call res with no response and status 204', () => {
      ResponseHelper.noContent(res);

      expect(statusSpy).to.have.been.calledOnce;
      expect(statusSpy).to.have.been.calledWith(204);
      expect(sendSpy).to.have.been.calledOnce;
      expect(sendSpy).to.have.been.calledWith();
    });
  });
});
