import database from '../../database/models';

class MemberService {
  static async getAllByOrgName(orgName) {
    try {
      return await database.Member.findAll({
        where: {
          orgName: orgName,
          deletedAt: null
        },
        attributes: ['username', 'avatarUrl', 'followersCount', 'followingCount'],
        order: [
          ['followersCount', 'DESC']
        ]
      });
    } catch (error) {
      throw error;
    }
  }
}

export default MemberService;
