import MemberService from '../services/MemberService';
import ResponseHelper from '../helpers/ResponseHelper';
import {ERROR_CODES} from "../constants/ErrorResponses";

class MemberController {
  static async getAllByOrgName(req, res) {
    try {
      const members = await MemberService.getAllByOrgName(req.params.orgName);
      ResponseHelper.jsonSuccess(res, members);
    } catch (error) {
      ResponseHelper.error(res, ERROR_CODES.INTERNAL_SERVER_ERROR);
    }
  }
}

export default MemberController;
