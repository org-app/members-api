import { Router } from 'express';
import MemberController from '../controllers/MemberController';

const router = Router();

router.get('/:orgName/members', MemberController.getAllByOrgName);

export default router;
